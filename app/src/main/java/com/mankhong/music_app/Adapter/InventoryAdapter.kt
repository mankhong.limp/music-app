package com.mankhong.music_app.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.mankhong.music_app.R
import com.mankhong.music_app.model.Song

class InventoryAdapter(val context: Context, private val songList:List<Song>):
    RecyclerView.Adapter<InventoryAdapter.InventoryViewHolder>(){
    class InventoryViewHolder(private val view: View):
        RecyclerView.ViewHolder(view){
        val image : ImageView = view.findViewById(R.id.songImage)
        val name  : TextView = view.findViewById(R.id.songName)
        val artist: TextView = view.findViewById(R.id.songArtist)
        val type  : TextView = view.findViewById(R.id.SongType)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InventoryViewHolder {
        val adapterLayout = LayoutInflater.from(
            parent.context
        ).inflate(R.layout.inventory_list,parent,false)
        return InventoryViewHolder(adapterLayout)
    }

    override fun onBindViewHolder(holder: InventoryViewHolder, position: Int) {
        val item = songList[position]
        holder.image.setImageResource(item.imageResourceId)
        holder.name.text = item.name
        holder.artist.text = item.artist
        holder.type.text = item.type

    }

    override fun getItemCount() = songList.size
}