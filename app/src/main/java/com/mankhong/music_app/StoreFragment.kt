package com.mankhong.music_app

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mankhong.music_app.Adapter.StoreAdapter
import com.mankhong.music_app.data.DataSourse
import com.mankhong.music_app.databinding.FragmentStoreBinding


class StoreFragment : Fragment() {
    private var _binding: FragmentStoreBinding? = null
    private val binding get() = _binding
    private lateinit var recyclerView: RecyclerView
    private var isLinearLayoutManager = true


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as AppCompatActivity).supportActionBar!!.title = "Shop"
        _binding = FragmentStoreBinding.inflate(inflater,container,false)
        val view = binding?.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val data = DataSourse.songs
        recyclerView = binding?.ItemView!!
        recyclerView.adapter = StoreAdapter(requireContext(),data){
            val action = StoreFragmentDirections.actionStoreFragmentToDetailFragment(id = it.id)
            view.findNavController().navigate(action)
        }
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        binding?.button?.setOnClickListener{
            val action = StoreFragmentDirections.actionStoreFragmentToInventoryFragment()
            view.findNavController().navigate(action)
        }
    }

    companion object {
    }
}