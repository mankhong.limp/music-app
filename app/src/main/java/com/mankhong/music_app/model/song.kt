package com.mankhong.music_app.model

data class Song (
    val id: Int,
    val imageResourceId: Int,
    val name: String,
    val artist: String,
    val type: String,
    val price: Int,
    val releaseDate: String,
    var bought: String
)

