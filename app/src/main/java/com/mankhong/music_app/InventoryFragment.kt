package com.mankhong.music_app

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mankhong.music_app.Adapter.InventoryAdapter
import com.mankhong.music_app.Adapter.StoreAdapter
import com.mankhong.music_app.data.DataSourse
import com.mankhong.music_app.data.InventoryData
import com.mankhong.music_app.databinding.FragmentInventoryBinding
import com.mankhong.music_app.databinding.FragmentStoreBinding


class InventoryFragment : Fragment() {
    private var _binding: FragmentInventoryBinding? = null
    private val binding get() = _binding
    private lateinit var recyclerView: RecyclerView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as AppCompatActivity).supportActionBar!!.title = "Inventory"
        _binding = FragmentInventoryBinding.inflate(inflater,container,false)
        return binding?.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val data = InventoryData.inventory_song
        recyclerView = binding?.ItemView!!
        recyclerView.adapter = InventoryAdapter(requireContext(),data)
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        binding?.StoreButton?.setOnClickListener{
            val action = InventoryFragmentDirections.actionInventoryFragmentToStoreFragment()
            view.findNavController().navigate(action)
        }
    }

    companion object {
    }
}